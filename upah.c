#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main()
{
  system("clear");
  
  char Nama[20];
  strcpy(Nama,"");
  char Gol = ' ';
  int JmlAnak = 0;
  float Upahkotor = 0.0,upahbersih=0.0;
  float PersenTunjangan = 0.0;

  printf("Nama: ");scanf("%s",Nama);
  printf("Gol (A - D) : ");scanf(" %c",&Gol);
  printf("Jumlah Anak : ");scanf("%d",&JmlAnak);

  switch (Gol)
  {
  case 'A':
    Upahkotor = 1000000;
    break;
  case 'B':
    Upahkotor = 800000;
    break;
  case 'C':
    Upahkotor = 600000;
    break;    
  case 'D':
    Upahkotor = 400000;
    break;

  default:
    Upahkotor = 0;
    break;
  } 
  
  if(JmlAnak>2)
  {
     PersenTunjangan = 0.3;
  }else{
     PersenTunjangan = 0.2;
  }
  upahbersih = Upahkotor - (Upahkotor * PersenTunjangan);
  printf("Upah: %10.2f\n",upahbersih);


  return 0;
}
