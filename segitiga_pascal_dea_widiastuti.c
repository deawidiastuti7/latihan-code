#include <stdio.h>

int faktorial(int n) {
    int f = 1;
    for(int i=1; i<=n; i++){
        f *= i;
    }
   return f;
}
int kombinasi(int n,int r){
    return faktorial(n) / (faktorial(n-r) * faktorial(r));
}
int main(){
    
int n,i,j,k;
printf("Masukkan N: ");
scanf("%d", &n);

for(i=0; i<n; i++){
    printf(" ");

    for(j=0; j<=n-i; j++){
        printf(" ");
    }

for(k=0; k<=i; k++){
    if(k==0 || k==i){
        printf("%d",kombinasi(i, k));
        printf(" ");
    }
    else{
        printf("%d",kombinasi(i, k));
        printf(" ");
    }
}
printf("\n");
}
    return 0;
}
